<?php

namespace HTM\FILMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


use HTM\FILMBundle\Entity\Actor;
use HTM\FILMBundle\Form\Type\ActorType;
use HTM\FILMBundle\Form\Type\ActorsDeleteType;



class ActorsController extends Controller{
    
    private $delete_token_name = "delete-film-%d";
    
 /**
     * @Route(
     *      "/list/{status}/{page}", 
     *      name="film_actorList",
     *      requirements={"page"="\d+"},
     *      defaults={"status"="all", "page"=1}
     * )
     * 
     * @Template()
     */
    public function indexAction(Request $request, $status, $page) {
        
        $ActorRepository = $this->getDoctrine()->getRepository('HTMFILMBundle:Actor');
        
        $qb = $ActorRepository->getQueryBuilder();
        
        $limit = $this->container->getParameter('admin.pagination_limit');
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $limit);
        
        $pagination->setCustomParameters(array(
            'class' => 'pagination',
            'span_class' => 'whatever'
        ));
        
        return array(
            'currPage' => 'actors',
            'pagination' => $pagination
        );
        
    }
    
    /**
     * @Route(
     *      "/form/{id}", 
     *      name="film_actorsForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL}
     * )
     * 
     * @Template()
     */
    public function formAction(Request $Request, Actor $Actor = NULL) {
        
        if(NULL === $Actor){
            $Actor = new Actor();
            $newActor = TRUE;
        } 
        
          $form = $this->createForm(new ActorType(), $Actor);
          $form->handleRequest($Request);
        
        if($form->isValid()){
                
            $em = $this->getDoctrine()->getManager();
            $em->persist($Actor);
            $em->flush();

            $message = (isset($newActor)) ? 'Poprawnie dodano nowego aktora!': 'Poprawiono dane aktora';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirect($this->generateUrl('film_actorsForm', array(
                'id' => $Actor->getId()
            )));
        }
        
        return array(
            'currPage' => 'taxonomies',
            'form' => $form->createView(),
            'actors' => $Actor
        );
        
    }    


    /**
     * @Route(
     *      "/delete/{id}", 
     *      name="film_actorsDelete"
     * )
     * 
     * @Template()
     */
    public function deleteAction(Request $Request, Actor $Actor) {
        
       $form = $this->createForm(new ActorsDeleteType($Actor));
        
        $form->handleRequest($Request);
        
        if($form->isValid()){
            
            if(true === $form->get('setNull')->getData()){
                $newActorsId = null;
                $chosen = true;
            }else if(null !== ($NewActor = $form->get('newCategory')->getData())){
                $newActorsId = $NewActor->getId();
                $chosen = true;
            }
            
            $FilmRepo = $this->getDoctrine()->getRepository('HTMFILMBundle:Film');
            $modifiedFilms = $FilmRepo->moveToCategory($Actor->getId(), $newActorsId);
            
            
               $em = $this->getDoctrine()->getManager();
                $em->remove($Actor);
                $em->flush();
                
                $Request->getSession()
                        ->getFlashBag()
                        ->add('success', sprintf('Kategoria została usunięta. %d filmów zostało zmodyfikowanych.', $modifiedFilms));
                
                return $this->redirect($this->generateUrl('film_actorsList'));
                
            
            
        }          
        
        return array(
            'currPage' => 'actors',
            'actors' => $Actor,
            'form' => $form->createView()
        );
    }
    
    
    
    
}
