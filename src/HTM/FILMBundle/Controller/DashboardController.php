<?php

namespace HTM\FILMBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DashboardController extends Controller
{
    /**
     * @Route(
     *      "/", 
     *      name="film_dashboard"
     * )
     * 
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
}
