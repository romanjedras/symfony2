<?php

namespace HTM\FILMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


use HTM\FILMBundle\Entity\Film;
use HTM\FILMBundle\Form\Type\FilmType;



class FilmsController extends Controller{
    
    private $delete_token_name = "delete-film-%d";
    
 /**
     * @Route(
     *      "/list/{status}/{page}", 
     *      name="film_postsList",
     *      requirements={"page"="\d+"},
     *      defaults={"status"="all", "page"=1}
     * )
     * 
     * @Template()
     */
    public function indexAction(Request $request, $status, $page) {
        
        $queryParams = array(
            'titleLike' => $request->query->get('titleLike'),
            'categoryId' => $request->query->get('categoryId'),
            'status' => $status
        ); 
        
        $FilmRepository = $this->getDoctrine()->getRepository('HTMFILMBundle:Film');
        
        $statistics = $FilmRepository->getStatistics();
        
        $qb = $FilmRepository->getQueryBuilder($queryParams);
        
        $paginationLimit = $this->container->getParameter('admin.pagination_limit');
        $limits = array(2, 5, 10, 15);
        
        $limit = $request->query->get('limit', $paginationLimit);
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb, $page, $limit);
        $pagination->setTemplate('HTMFILMBundle:Pagination:pagination.html.twig');

       $categoriesList = $this->getDoctrine()->getRepository('HTMFILMBundle:Category')->getAsArray();
        $statusesList = array(
            'Wszystkie' => 'all',
            'Opublikowane' => 'published',
            'Nieopublikowane' => 'unpublished'
        );
        
        
        
        
     return array(
         'currPage' => 'films',
         'queryParams' => $queryParams,
            'categoriesList' => $categoriesList,
            
            'limits' => $limits,
            'currLimit' => $limit,
            
            'statusesList' => $statusesList,
            'currStatus' => $status,
            'statistics' => $statistics,
            
            'pagination' => $pagination,
            'currLimit' => $limit,
            'currStatus' => $status,
            
            'deleteTokenName' => $this->delete_token_name,
            'csrfProvider' => $this->get('form.csrf_provider')
         );   
    }

    /**
     * @Route(
     *      "/form/{id}", 
     *      name="film_postForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL}
     * )
     * 
     * @Template()
     */
   public function formAction(Request $Request, Film $Film = NULL) {
    
         if(NULL === $Film){
            $Film = new Film();
            
            if(!empty($this->getUser())){
            $Film->setAuthor($this->getUser());    
            }
            $newFilmForm = TRUE;
        }
        
        $form = $this->createForm(new FilmType(), $Film);
        $form->handleRequest($Request);
        
        if($form->isValid()){
                
            $em = $this->getDoctrine()->getManager();
            $em->persist($Film);
            $em->flush();
            
           $message = (isset($newFilmForm)) ? 'Poprawnie dodano nowy film!': 'Film został poprawiony!';
            $this->get('session')->getFlashBag()->add('success', $message);

           return $this->redirect($this->generateUrl('film_postForm', array(
                'id' => $Film->getId()
            )));  
           
      }
        return array(
            'currPage' => 'films',
            'form' => $form->createView(),
            'film' => isset($Film) ? $Film : NULL 
        );
   }
   
    /**
     * @Route(
     *      "/delete/{id}/{token}", 
     *      name="film_postDelete",
     *      requirements={"id"="\d+"}
     * )
     * 
     * @Template()
     */
    public function deleteAction($id, $token) {
        
        $tokenName = sprintf($this->delete_token_name, $id);
        $csrfProvider = $this->get('form.csrf_provider');
        
        if(!$csrfProvider->isCsrfTokenValid($tokenName, $token)){
            $this->get('session')->getFlashBag()->add('error', 'Niepoprawny token akcji!');
            
        }else{
            
            $Film = $this->getDoctrine()->getRepository('HTMFILMBundle:Film')->find($id);
            $em = $this->getDoctrine()->getManager();
            $em->remove($Film);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Poprawnie usunięto film');
        }
        
        return $this->redirect($this->generateUrl('film_postsList'));
    }
   
   
}
