<?php

namespace HTM\FILMBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use HTM\FILMBundle\Entity\Actor;



/**
 * Description of FilmsFixtures
 *
 * @author User
 */
class ActorsFixtures extends AbstractFixture implements OrderedFixtureInterface {
    //put your code here
    public function getOrder(){
        return 0;
    }

    public function load(ObjectManager $manager) {
        
        $actorsList = array(
            array(
                'name' => 'Piotr Adamczyk',
                'dateasistance'=> '2012-01-01 12:12:12',
                'sex' => 'men',
                'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => '2012-01-01 12:12:12',
                ),
            array(
                'name' => 'Jerzy Sthur',
                'dateasistance'=> '2012-01-01 12:12:12',
                'sex' => 'men',
                'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => '2012-01-01 12:12:12',
                ),
           array(
                'name' => 'Marek Konrad',
                'dateasistance'=> '2012-01-01 12:12:12',
                'sex' => 'men',
                'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => '2012-01-01 12:12:12',
                ), 
        );
        
        
        foreach ($actorsList as $idx => $details) {
            $Actor = new Actor();
            
            $Actor->setName($details['name'])
                    ->setDateasistance(new \DateTime($details['createDate']))
                    ->setSex($details['sex'])
                    ->setCreateDate($details['createDate'])
                    ->setCreateDate(new \DateTime($details['createDate']));
                    
             if(null !== $details['publishedDate']){
                $Actor->setPublishedDate(new \DateTime($details['publishedDate']));
            }
          
            $this->setReference('actor_'.$idx, $Actor);

                 $manager->persist($Actor);
        }
        

        $manager->flush();
    }

}
