<?php

namespace HTM\FILMBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use HTM\FILMBundle\Entity\Category;


class CategoriesFixtures extends AbstractFixture implements OrderedFixtureInterface{
   
    public function getOrder(){
        return 0;
    }

    public function load(ObjectManager $manager) {
        
        $categoriesList = array(
            'komedie' => 'Komedie',
            'kryminalne' => 'Kryminalne',
            'dramaty' => 'Dramaty',
            'wojenne' => 'Wojenne',
            'komedie-kryminalne' =>'Komedie kryminalne',
            'romance' => 'Romance',
            'przygodowe'=>'Przygodowe',
            'kosmiczne'=>'Kosmiczne',
            'tajne'=>'Tajne'
            );
        
        foreach ($categoriesList as $key => $name) {
            $Category = new Category();
            $Category->setName($name)
                     ->setSlug(str_replace(' ', '-', ucwords($name)));
            
            $manager->persist($Category);
            $this->setReference('category_'.$key, $Category);
        }
        $manager->flush();
        
    }

}
