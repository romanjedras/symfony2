<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace HTM\FILMBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use HTM\FILMBundle\Entity\Page;


class PagesFixtures extends AbstractFixture implements OrderedFixtureInterface{
    
    public function getOrder(){
        return 2;
    }

    public function load(ObjectManager $manager) {
     
        $pagesList = array(
            array(
                'title' => 'O nas',
                'content' => '<p>Vestibulum non ipsum. Curabitur egestas. Integer hendrerit purus consectetuer adipiscing elit. In sodales in, elementum vel, velit. Suspendisse fermentum, metus convallis nec, hendrerit purus lorem, pellentesque consectetuer, augue a quam purus, pharetra varius. In nec massa. Nam dolor non ipsum. Duis lobortis, massa ut porta sapien leo vitae turpis. Duis ante et velit. Suspendisse a arcu. Sed nibh rutrum rhoncus. Morbi vel turpis luctus tellus ante nec enim fringilla et, congue ac, blandit vestibulum tincidunt justo. Nulla congue. Nam pharetra nec, molestie lorem velit ornare dolor dictum sed, congue sit amet ligula. Vivamus euismod. Vestibulum quam. Aliquam adipiscing tortor, a wisi. Suspendisse turpis a dolor. Fusce blandit risus auctor consectetuer. Sed posuere cubilia Curae, Donec pulvinar ut, gravida non, porttitor lectus urna ullamcorper ut, metus. Nulla quis est. Nunc viverra, enim dictum a, mauris. Praesent odio sagittis nibh nulla, faucibus orci ac metus. Curabitur tempor. Suspendisse turpis viverra vel, urna. Cras a dolor. Mauris interdum vitae, vestibulum volutpat, erat ut enim. In quam accumsan id, bibendum porttitor. Aenean egestas dignissim, libero mollis aliquam, nulla facilisis urna eu pede ultrices tortor ante ullamcorper massa placerat at, velit. Mauris convallis ligula accumsan nisl a augue. Donec faucibus scelerisque. Nam turpis et augue. Nulla augue ut arcu. In malesuada velit eleifend ut, ligula. Mauris ut porta tellus ac arcu. Duis ut justo a dolor sit amet tellus sodales pede. Morbi nibh. Morbi risus metus eros sed tortor. In ornare eu, fringilla mi, viverra luctus, enim est eu.</p>',
                'category' => 'komedie',
               'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => NULL,
            ),
            array(
                'title' => 'Strona testowa1',
                'content' => '<p>Suspendisse fermentum, metus convallis nec, hendrerit purus lorem, pellentesque consectetuer, augue a quam purus, pharetra varius. In nec massa. Nam dolor non ipsum. Duis lobortis, massa ut porta sapien leo vitae turpis. Duis ante et velit. Suspendisse a arcu. Sed nibh rutrum rhoncus. Morbi vel turpis luctus tellus ante nec enim fringilla et, congue ac, blandit vestibulum tincidunt justo. Nulla congue. Nam pharetra nec, molestie lorem velit ornare dolor dictum sed, congue sit amet ligula. Vivamus euismod. Vestibulum quam. Aliquam adipiscing tortor, a wisi. Suspendisse turpis a dolor. Fusce blandit risus auctor consectetuer. Sed posuere cubilia Curae, Donec pulvinar ut, gravida non, porttitor lectus urna ullamcorper ut, metus. Nulla quis est. Nunc viverra, enim dictum a, mauris. Praesent odio sagittis nibh nulla, faucibus orci ac metus. Curabitur tempor. Suspendisse turpis viverra vel, urna. Cras a dolor. Mauris interdum vitae, vestibulum volutpat, erat ut enim. In quam accumsan id, bibendum porttitor. Aenean egestas dignissim, libero mollis aliquam, nulla facilisis urna eu pede ultrices tortor ante ullamcorper massa placerat at, velit. Mauris convallis ligula accumsan nisl a augue. Donec faucibus scelerisque. Nam turpis et augue. Nulla augue ut arcu. In malesuada velit eleifend ut, ligula. Mauris ut porta tellus ac arcu. Duis ut justo a dolor sit amet tellus sodales pede. Morbi nibh. Morbi risus metus eros sed tortor. In ornare eu, fringilla mi, viverra luctus, enim est eu.</p>',
                'category' => 'komedie',
                'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => '2012-11-04 12:10:11',
            ),
            array(
                'title' => 'Strona testowa2',
                'content' => '<p>Lorem ipsum dolor sit amet purus at adipiscing vitae, vulputate vitae, vestibulum lorem lorem lectus, pellentesque auctor tortor elit a arcu. Etiam sit amet nunc tempus ac, rhoncus ut, magna. Integer tristique magna nec nunc ultrices adipiscing non, posuere in, libero. Nulla facilisi. Nam laoreet hendrerit sagittis. Curabitur ut metus. Aliquam eleifend, ligula. Curabitur nunc. Aliquam ultricies porta. Sed eros. Pellentesque facilisis. Fusce et arcu. In hac habitasse platea dictumst. Vestibulum ante vitae erat libero, consectetuer dignissim, sapien varius quis, velit. Suspendisse fringilla orci. Mauris in vestibulum id, vulputate aliquam id, tortor. Cras id wisi diam, venenatis quis, ornare velit wisi, ullamcorper ligula nunc, rhoncus et, imperdiet aliquam ut, nunc. Sed porta neque. Vestibulum et pede sit amet, ante. Vivamus arcu sed laoreet fermentum. Proin gravida at, egestas vitae, vulputate tellus a dui. Mauris nec tellus. Donec molestie mauris. Donec sit amet eleifend purus laoreet hendrerit nunc ac erat velit odio at sollicitudin orci. Integer condimentum justo non mattis egestas, justo ac turpis nec augue. Donec libero nec lectus elit, ut justo. Donec porta, metus et interdum dui gravida pulvinar eget, velit. Mauris eget luctus aliquam, risus. Sed in dolor. Mauris lobortis vitae, sollicitudin lorem, id mauris ac lacus. Nulla mi odio, in faucibus justo imperdiet quis, vestibulum nec, sagittis tortor quis justo. In hac habitasse platea dictumst. Nunc leo. Sed pharetra. Suspendisse elit. Curabitur sed tortor. Ut turpis. Nam eget imperdiet dignissim, tellus. Integer lacinia eget, pede. Cras luctus diam eros, aliquam vehicula convallis varius. Ut sit amet leo.</p>',
                'category' => 'komedie',
                'createDate' => '2012-01-01 12:12:12',
                'publishedDate' => '2012-12-23 16:10:11',
            )
        );
        
        foreach ($pagesList as $idx => $details) {
            $Page = new Page();
            
            $Page->setTitle($details['title'])
                    ->setSlug(strtolower(str_replace(' ','-',$details['title'])))
                    ->setContent($details['content'])
                    ->setCreateDate($details['createDate'])
                    ->setCreateDate(new \DateTime($details['createDate']));
                    
             if(null !== $details['publishedDate']){
                $Page->setPublishedDate(new \DateTime($details['publishedDate']));
            }
          
            
            $Page->setCategory($this->getReference('category_'.$details['category']));
            
            
            $this->addReference('page-'.$idx, $Page);

                 $manager->persist($Page);
        }
        

        $manager->flush();
        
        
    }

}
