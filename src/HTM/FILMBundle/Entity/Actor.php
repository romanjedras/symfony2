<?php

namespace HTM\FILMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FSi\DoctrineExtensions\Uploadable\Mapping\Annotation as FSi;
use FSi\Bundle\DoctrineExtensionsBundle\Validator\Constraints as FSiAssert;


/**
 * Actor
 *
 * @ORM\Table(name="actors")
 * @ORM\Entity(repositoryClass="HTM\FILMBundle\Repository\ActorRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields={"name"})
 * 
 * 
 * 
 */
class Actor
{
    
    const DEFAULT_AVATAR = 'dota_crystalmaiden.jpg';
    const UPLOAD_DIR = 'uploads/movie/actors/';
    
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateasistance", type="datetime")
     */
    private $dateasistance;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=255)
     */
    private $sex;

    
     /**
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;
    
    /**
     * @ORM\Column(name="published_date", type="datetime", nullable=true)
     * 
     * @Assert\DateTime
     */
    private $publishedDate = null;    
    
    /**
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate = null;
            
      
    /**
     * @ORM\Column(length=255, nullable=true, name="photo_key")
     * @FSi\Uploadable(targetField="thumbnailFile")
     */
    protected $photoKey;
    
    
      /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $thumbnail = null;
    
    /**
     * @Assert\Image(
     *      minWidth = 60,
     *      minHeight = 60,
     *      maxWidth = 1920,
     *      maxHeight = 1080,
     *      maxSize = "1M"
     * )
     */
    private $thumbnailFile;
    
    private $thumbnailTemp;        
            
            
    
    protected $category;
    
    
    /**
     *
     * @ORM\ManyToMany(
     *      targetEntity = "Film",
     *      mappedBy = "actors"
    * )
     * 
     */
    
    
    protected $films;
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->films = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    

  

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Actor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateasistance
     *
     * @param \DateTime $dateasistance
     * @return Actor
     */
    public function setDateasistance($dateasistance)
    {
        $this->dateasistance = $dateasistance;

        return $this;
    }

    /**
     * Get dateasistance
     *
     * @return \DateTime 
     */
    public function getDateasistance()
    {
        return $this->dateasistance;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Actor
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Actor
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set publishedDate
     *
     * @param \DateTime $publishedDate
     * @return Actor
     */
    public function setPublishedDate($publishedDate)
    {
        $this->publishedDate = $publishedDate;

        return $this;
    }

    /**
     * Get publishedDate
     *
     * @return \DateTime 
     */
    public function getPublishedDate()
    {
        return $this->publishedDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Actor
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Add films
     *
     * @param \HTM\FILMBundle\Entity\Film $films
     * @return Actor
     */
    public function addFilm(\HTM\FILMBundle\Entity\Film $films)
    {
        $this->films[] = $films;

        return $this;
    }

    /**
     * Remove films
     *
     * @param \HTM\FILMBundle\Entity\Film $films
     */
    public function removeFilm(\HTM\FILMBundle\Entity\Film $films)
    {
        $this->films->removeElement($films);
    }

    /**
     * Get films
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilms()
    {
        return $this->films;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Actor
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        if(null == $this->thumbnail){
            return Actor::UPLOAD_DIR.Actor::DEFAULT_AVATAR;
        }
        
        return Actor::UPLOAD_DIR.$this->thumbnail;
    }
    
    function getThumbnailFile() {
        return $this->thumbnailFile;
    }

    function getThumbnailTemp() {
        return $this->thumbnailTemp;
    }

      public function setThumbnailFile(UploadedFile $thumbnailFile) {
        $this->thumbnailFile = $thumbnailFile;
        $this->updateDate = new \DateTime();
        return $this;
    }

    function setThumbnailTemp($thumbnailTemp) {
        $this->thumbnailTemp = $thumbnailTemp;
    }
    
     /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preSave(){
       
        
        if(null !== $this->getThumbnailFile()){
            
            if(null !== $this->thumbnail){
                $this->thumbnailTemp = $this->thumbnail;
            }
            
            $fileName = sha1(uniqid(null, true));
            $this->thumbnail = $fileName.'.'.$this->getThumbnailFile()->guessExtension();
        }
        
        if(null == $this->createDate){
            $this->createDate = new \DateTime();
        }
    }
    
    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function postSave(){
        if(NULL !== $this->getThumbnailFile()){
            $this->getThumbnailFile()->move($this->getUploadRootDir(), $this->thumbnail);
            unset($this->thumbnailFile);
            
            if(isset($this->thumbnailTemp)){
                unlink($this->getUploadRootDir().'/'.$this->thumbnailTemp);
                unset($this->thumbnailTemp);
            }
        }
    }
    
   
    
    public function getUploadRootDir(){
        return __DIR__.'/../../../../web/'.self::UPLOAD_DIR;
    }


    public function getAbsolutePath(){
        
        return null === $this->thumbnail ? null : $this->getUploadRootDir().'/'.$this->thumbnail;
        
    }
    
    /**
     * @param mixed $photoKey
     * @return Actor
     */
    public function setPhotoKey($photoKey)
    {
        $this->photoKey = $photoKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhotoKey()
    {
        return $this->photoKey;
    }
    
}
