<?php

namespace HTM\FILMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;




/**
 * Category
 *
 * @ORM\Table(name="films_categorie")
 * @ORM\Entity(repositoryClass="HTM\FILMBundle\Repository\CategorieRepository")
 */
class Category extends AbstractTaxonomy
{
    
    /**
     *
     * @ORM\OneToMany(
     *      targetEntity = "Film",
     *      mappedBy = "category"
     * 
     * )
     */
    
    
    protected $films;
    
    
    /**
     * Get films
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilms()
    {
        return $this->films;
    }

    /**
     * Add films
     *
     * @param \HTM\FILMBundle\Entity\Film $films
     * @return Category
     */
    public function addFilm(\HTM\FILMBundle\Entity\Film $films)
    {
        $this->films[] = $films;

        return $this;
    }

    /**
     * Remove films
     *
     * @param \HTM\FILMBundle\Entity\Film $films
     */
    public function removeFilm(\HTM\FILMBundle\Entity\Film $films)
    {
        $this->films->removeElement($films);
    }
}
