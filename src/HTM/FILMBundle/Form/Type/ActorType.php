<?php

namespace HTM\FILMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;




class ActorType extends AbstractType{
    
    public function getName(){
        return 'actor';
    }
    
      public function buildForm(FormBuilderInterface $builder, array $options)
    {
          
         
          
        $builder
            ->add('name', 'text', array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Tytuł'
                )
            ))
            ->add('thumbnailFile', 'file', array(
                'label' => 'Miniaturka'
            ))
            ->add('dateasistance', 'datetime', array(
                'label' => 'Data premierowa',
                'date_widget' => 'single_text', //pokazać też bez
                'time_widget' => 'single_text'
            )) 
            ->add('sex', 'text', array(
                'label' => 'Płeć',
                'attr' => array(
                    'placeholder' => 'Płeć'
                )
            ))
            ->add('publishedDate', 'datetime', array(
                'label' => 'Data publikacji',
                'date_widget' => 'single_text', //pokazać też bez
                'time_widget' => 'single_text'
            ))
           ->add('save','submit',array('label'=>'Zapisz'));      
                
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HTM\FILMBundle\Entity\Actor'
        ));
    }
    
    

}
