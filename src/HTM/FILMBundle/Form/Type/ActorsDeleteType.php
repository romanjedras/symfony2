<?php

namespace HTM\FILMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActorsDeleteType extends AbstractType {
    
    private $actors;

    function __construct($actors) {
        $this->actors = $actors;
    }

    public function getName() {
        return 'deleteActors';
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $Actors = $this->actors;
        
        $builder
          ->add('newActors', 'entity', array(
                'label' => 'Wybierz aktora',
                'empty_value' => 'Wybierz kategorię',
                'class' => 'HTM\FILMBundle\Entity\Actor',
                'property' => 'name',
                'query_builder' => function(EntityRepository $er) use ($Actors){
                    return $er->createQueryBuilder('a');
                }
            ))
            ->add('submit', 'submit', array(
                    'label' => 'Usuń aktora'
            ));
    }
    
 public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HTM\FILMBundle\Entity\Actor'
        ));
    }   
    
}
