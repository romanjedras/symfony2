<?php

namespace HTM\FILMBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FilmType extends AbstractType
{

    public function getName()
    {
        return 'film';
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'label' => 'Tytuł',
                'attr' => array(
                    'placeholder' => 'Tytuł'
                )
            ))
             ->add('slug', 'text', array(
                'label' => 'Alias',
                'attr' => array(
                    'placeholder' => 'Alias'
                )
            ))
            ->add('content', 'ckeditor', array(
                'label' => 'Treść'
            ))
            ->add('thumbnailFile', 'file', array(
                'label' => 'Miniaturka'
            ))
            ->add('publishedDate', 'datetime', array(
                'label' => 'Data publikacji',
                'date_widget' => 'single_text', //pokazać też bez
                'time_widget' => 'single_text'
            ))
            ->add('category', 'entity', array(
                'label' => 'Kategoria',
                'class' => 'HTM\FILMBundle\Entity\Category',
                'property' => 'name',
                'empty_value' => 'Wybierz kategorię'
            ))    
            ->add('actors', 'entity', array(
                'label' => 'Tagi',
                'multiple' => true,
                'class' => 'HTM\FILMBundle\Entity\Actor',
                'property' => 'name',
                'attr' => array(
                    'placeholder' => 'Dodaj tagi'
                )
            ))
            ->add('save', 'submit', array(
                'label' => 'Zapisz'
            ));    
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HTM\FILMBundle\Entity\Film'
        ));
    }
    

    
}    

