<?php

namespace HTM\FILMBundle\Repository;

use Doctrine\ORM\EntityRepository;


class TaxonomyRepository extends EntityRepository {
    
    public function getQueryBuilder(array $params = array()) {
        
        $qb = $this->createQueryBuilder('t');
        
        $qb->select('t, t.name, COUNT(f.id) as filmsCount')
                ->leftJoin('t.films', 'f')
                ->groupBy('t.id');
        
     
        
      return $qb;

    }
    
    public function getAsArray() {
        return $this->createQueryBuilder('c')
                        ->select('c.id, c.name')
                        ->getQuery()
                        ->getArrayResult();
    }
    
}
